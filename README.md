Usage:
    `$ rusty-pirate refresh`

Downloads new mirrors

To search for "a query string"

    `$ rusty-pirate sail -w /path/to/dwld/folder a query string`

Results will be prompted in an indexed table. Enter a list of numbers
 (ie. 0 2 17 3) to add the corresponding magnets to your torrenting program.

The torrenter program is configured through the command.file config. Right now
it only supports adding torrents as long as the torrenter syntax goes like this
 (transmission-remote as model):

    `$ torrentmanager --dwld-dir /full/path/to/download/dir --addflag magnet`

The full path is necessary for transmission-remote as apparently it doesn't 
support "~"-expansion.
