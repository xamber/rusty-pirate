use std::fs;
use std::path;

fn main() {
    // Add git commit version to build
    let ref_file  = fs::read_to_string("./.git/refs/heads/master");
    match ref_file {
        Ok(hash) => {
            println!("{}{}","cargo:rustc-env=GIT_COMMIT_HASH=",&hash);
            println!("{}{}","cargo:rustc-env=GIT_COMMIT_HASH_SHORT=",&hash[0..9]);
        }
        Err(_) => {
            println!("{}{}","cargo:rustc-env=GIT_COMMIT_HASH=","");
            println!("{}{}","cargo:rustc-env=GIT_COMMIT_HASH_SHORT=","");
        }
    }


    let home_path = path::Path::new(env!("HOME"));
    let config_dir = home_path.join(".config/rusty_pirate");
    println!("cargo:rustc-env=CONFIG_DIR={}",config_dir.to_str().unwrap());
}
