extern crate reqwest;
extern crate scraper;

use scraper::{Html, Selector, html::Select};

pub struct Filter {
    frag : Html,
    filt : Selector,
}
impl Filter {
    // parses based on a CSS selector
    pub fn from(html_string : &str, s : &str) -> Filter {
        let fragment = Html::parse_fragment(&html_string);
        Filter {
            frag : fragment,
            filt : Selector::parse(s).unwrap(),
        }
    }

    pub fn select(&self) -> Select {
        self.frag.select(&self.filt)
    }
}

#[derive(PartialEq)]
pub enum RequestResult {
    Found(String),
    NotFound,
}

impl RequestResult {

    fn make_request(url : &str) -> Result<reqwest::Response, reqwest::Error> {

        let rpolicy = reqwest::RedirectPolicy::limited(1usize);

        let client = reqwest::Client::builder()
            .redirect(rpolicy)
            .build()?;

        let client_get = client.get(url).send();

        client_get
    }

    fn r_handler(response : &mut reqwest::Response ) -> RequestResult {
        match response.status() {
            reqwest::StatusCode::NotFound => {
                println!("Arriving at 404 sir, yer not lost arn't ye?");
                RequestResult::NotFound
            },
            reqwest::StatusCode::Unregistered(u) => {
                println!("Poseidon's beard! wut is {:?}?' ",u);
                println!("Possible server error");
                RequestResult::NotFound
            }
            _ => {
                let response_text  = response.text().unwrap();
                RequestResult::Found(response_text)
            }
        }
    }

    fn e_handler(error: reqwest::Error) -> RequestResult {
        println!("Wi'v lost'em in the storm"); 
        println!("error := {:?}", error.get_ref());
        if error.is_http() {
            match error.url() {
                None => println!("No Url given"),
                Some(url) => println!("Problem making request to: {}", url),
            }
        }
        // Inspect the internal error and output it
        if error.is_serialization() {
            if let Some(err) = error.get_ref() {
                println!("problem parsing information {}", err)
            }
        }
        if error.is_redirect() {
            println!("server redirecting too many times or making loop");
        }
        RequestResult::NotFound
    }

    pub fn request_errorhandler(query_url: &str) -> RequestResult {
        match RequestResult::make_request(&query_url) {
            Ok(mut response) => RequestResult::r_handler(&mut response),
            Err(e) => RequestResult::e_handler(e),
        }
    }
}


pub trait Torrent {
    fn torrent(&self) -> String;
}

pub mod parse {

    use std::io;

    pub fn read_input_from_cli () -> String {
        let mut user_input = String::new();
        io::stdin().read_line(&mut user_input)
            .expect("Failed to read line");
        user_input
    }

    pub fn sdasd() -> Result<String, &'static str> {
        let mut user_input = String::new();
        match io::stdin().read_line(&mut user_input) {
            Ok(_n) => Ok(user_input),
            Err(_) => Err("Input error")
        }
    }

    pub fn turn_string_to_query_url(query : &str, mirror : &str) -> String {
        let query_url : String = String::new();
        query_url + &mirror + "/search/" + &query + "/0/99/0"   // By relevance
            // query_url + &mirror + "/search/" + &query + "/1/3/0" // By date
            // query_url + "https://" + &mirror + "/search/" + &query + "/0/99/0"
    }

} /* parse */

pub mod types {

#[derive(Debug)]
    pub struct Page<T> {
        number : usize,
        display_size : usize,
        entry_quantity : usize,
        result_vec : Vec<T>
    }

    //public
    impl<T> Page<T> {

        pub fn new(d:usize) -> Page<T> {
            Page {
                number : 1usize,
                display_size : d,
                entry_quantity : 0usize,
                result_vec : Vec::new()
            }
        }

        pub fn from(d:usize,tl:Vec<T>) -> Page<T> {
            let m = tl.len();
            let size_ : usize = if m < d { m } else { d } ;
            Page {
                number : 1usize,
                display_size : size_,
                entry_quantity : m,
                result_vec : tl
            }
        }

        pub fn add_result(&mut self, t : T) {
            self.result_vec.push(t);
            self.entry_quantity += 1usize;
        }

        pub fn size(&self) -> usize {
            self.entry_quantity
        }

        pub fn total_pages(&self) -> usize {
            let f_total = self.entry_quantity as f32;
            let f_size = self.display_size as f32;
            (f_total/f_size).ceil() as usize // pages are one based
        }

        pub fn next(&mut self) {
            let n_page = self.number + 1;
            if n_page > self.total_pages() { // pages are one based
                println!("Already in last page!");
            }
            else {
                self.number = n_page
            }
        }

        pub fn prev(&mut self) {
            let p_page = self.number - 1;
            if p_page == 0usize {
                println!("Already in first page!");
            }
            else {
                self.number = p_page
            }
        }

        pub fn torrent_list(&self) -> &Vec<T> {
            &self.result_vec
        }

        pub fn display_size(&self) -> usize {
            self.display_size
        }

        pub fn number(&self) -> usize {
            self.number
        }

        pub fn is_empty(&self) -> bool {
            if self.entry_quantity == 0usize { true } else { false }
        }

        // pub fn remove(&mut self, index : usize) -> T {
        //     self.result_vec.remove_item(index)
        // }

        pub fn iter(self) -> PageIntoIterator<T> {
            PageIntoIterator {
                page : self.result_vec,
                index: 0usize
            }
        }

        pub fn nc_iter(&self) -> PageIterator<T> {
            PageIterator {
                vl : &self.result_vec,
                index: 0usize
            }
        }
    } /* end Page impl */

    // consuming
    pub struct PageIntoIterator<T> {
        page : Vec<T>,
        index : usize
    }

    impl<T> IntoIterator for Page<T> {
        type Item = T;
        type IntoIter = PageIntoIterator<T>;

        fn into_iter(self) -> Self::IntoIter {
            PageIntoIterator { page: self.result_vec, index: 0 }
        }
    }

    impl<T> Iterator for PageIntoIterator<T> {
        type Item = T;
        fn next(&mut self) -> Option<T> {
            if self.index < self.page.len() {
                let out = self.page.remove(self.index);
                self.index += 1;
                Some(out)
            }
            else {
                None
            }
        }
    }

    //// non consuming
    pub struct PageIterator<'a,T:'a> {
        vl : &'a Vec<T>,
        index : usize
    }

    impl<'a,T> IntoIterator for &'a Page<T> {
        type Item = &'a T;
        type IntoIter = PageIterator<'a,T>;

        fn into_iter(self) -> Self::IntoIter {
            PageIterator { vl: &self.result_vec, index: 0 }
            // self.torrent_list.into_iter()
        }
    }

    impl<'a,T> Iterator for PageIterator<'a,T> {
        type Item = &'a T;
        fn next(&mut self) -> Option<&'a T> {
            if self.index < self.vl.len() {
                let out = &self.vl[self.index];
                self.index += 1usize;
                Some(out)
                    // Some( &self.vl.get_unchecked(self.index) )
            }
            else {
                None
            }
        }
    }

    // SpVecT, we can loop it in arbritrary size slices
    pub struct SpVecT<T>(Vec<T>);

    impl<T> SpVecT<T> {
        pub fn from(v:Vec<T>) -> SpVecT<T>{
            SpVecT( v )
        }
    }

    pub struct SlVecTIterator<'a,T:'a> {
        r_slice : &'a [T],
        count : usize,
        quant : usize,
    }

    impl<'a,T> Iterator for SlVecTIterator<'a,T> {
        type Item = &'a [T];
        fn next(&mut self) -> Option<Self::Item> {
            if self.count < self.r_slice.len() {
                let iter = self.count;
                self.count += self.quant;
                if self.count < self.r_slice.len() {
                    Some( &self.r_slice[iter..self.count] )
                }
                else {
                    Some(  &self.r_slice[iter..self.r_slice.len()] )
                }
            }
            else {
                None
            }
        }
    }

    pub trait CIntoIterator {
        type Item;
        type IntoIter : Iterator<Item=Self::Item>;
        fn into_iter(self, size : usize) -> Self::IntoIter;
    }

    impl<'a,T> CIntoIterator for &'a SpVecT<T> {
        type Item = &'a [T];
        type IntoIter = SlVecTIterator<'a,T>;

        fn into_iter(self,size:usize) -> Self::IntoIter {
            SlVecTIterator { r_slice: &self.0[..], count: 0, quant:size }
        }
    }

} /* types */

pub mod command {

    use std;
    use std::io::{self, Write};
    use std::process;
    use std::fs;
    use std::iter::FromIterator;
    use std::collections::HashMap;
    use Torrent;
    use std::path;

    pub enum Action {
        Next,
        Prev,
        Info(Vec<usize>),
        Help,
        Quit,
        NumberList(Vec<usize>),
        Unknown(std::num::ParseIntError),
    }

    impl  Action  {

        pub fn to_info<'b>(list : &'b [&'b str]) -> Action  {
            let numlist : Vec<usize> = list[1..].into_iter()
                .filter_map(|s| s.parse::<usize>().ok())
                .collect();
            Action::Info(numlist)
        }

        pub fn parse_unknown(test_char : &str, list : &[&str]) -> Action  {
            match test_char.parse::<usize>() {
                Ok(_number) => { Action::NumberList(
                        list.into_iter()
                        .filter_map(|s| s.parse::<usize>().ok())
                        .collect())
                }
                Err(e)    => Action::Unknown(e),
            }
        }

        pub fn parse_input <'a>(input_string : &'a str) -> Action  {
            /* Parses user input and returns an ExpectedInput type */
            let list = input_string.trim().split(" ")
                .filter(|x| x.len()>0) // filters out empty strings
                .collect::<Vec<&str>>();
            let first_char = list.first().cloned();

            match first_char {
                Some(command) => match command {
                    "n"|"next"     => Action::Next,
                    "p"|"prev"     => Action::Prev,
                    "i"|"info"     => Action::to_info(&list),
                    "q"|"quit"     => Action::Quit,
                    "?"|"h"|"help" => Action::Help,
                    _              => Action::parse_unknown(&command,&list),//Action::Unknown(comm),
                },
                None    => Action::Help,
            }
        }

        pub fn print_help() {
            println!("
Commands:
n           Next page
p           Previous page
i           Result info
q           Quit program
?           Print this
#list       Are send to torrenter: eg. 0 3 1 17
");
            io::stdout().flush().unwrap();
        }
    } // end impl Action

    /// Reads command.file configuration to setup the torrenter commands.
    fn read_command_file() -> HashMap<String,String> {

        let config_path = path::Path::new(env!("CONFIG_DIR"));
        let file_text = fs::read_to_string(config_path.join("command.file"))
            .expect("Can't read file")
            .to_string();

        let mut command : HashMap<_,_> = HashMap::new();

        let lines = file_text.lines(); // splits &str into newlines
        for mut line in lines {

            let collection : Vec<&str> = line.split("=").collect();
            let mut coll = collection.to_vec();
            // command.push(coll[0])
            command.insert(coll[0].to_string(),coll[1].to_string());
        };

        command
    }

    /// Sends torrents to torrenter program. It needs the command.file file to 
    /// read the correct flags. 
    /// TODO: I'm pretty sure the result list can be parallized with some
    /// thread magic. Low priority for now
    pub fn send_to_torrent <A,I: IntoIterator<Item=A>>
        (numlist : Vec<usize>, list : I)
        -> Vec<io::Result<process::Output>>
        where A: std::fmt::Debug + Torrent {

            let cmd : HashMap<String,String> = read_command_file();
            let select = Vec::from_iter(list);
            let mut output_vec : Vec<io::Result<process::Output>> = Vec::new();
            for i in numlist {
                let item = &select[i];
                let mut _output = process::Command::new(&cmd["command"]);
                let _output = _output
                    .arg(&cmd["download_dir_cmd"]).arg(&cmd["download_dir"])
                    .arg(&cmd["add"]).arg(&item.torrent());
                println!("{:?}", _output);

                let output = _output.output(); //.unwrap();
                output_vec.push(output)
            }
            output_vec
        }
} /* command */
