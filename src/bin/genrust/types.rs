use std::str;
use scraper;
use unicode_segmentation::UnicodeSegmentation;
use bucaneer;
use prettytable::{Table, Slice, row::Row};
use bucaneer::Torrent;

type SSelect<'a,'b> = scraper::html::Select<'a,'b>;
type SSelectItem<'a,'b> = <SSelect<'a,'b> as Iterator>::Item;

pub trait Rowable {
    fn header() -> Row;
    fn col_values(&self) -> [&String; 5];
}

pub trait Tableable {
    type TRow;
    fn tablify (&self) -> Table;
}

pub trait Printable {
    fn print(&self);
}

#[derive(Debug)]
pub struct GenResult {
    id : String,
    author : String,
    title : String,
    publisher : String,
    year : String,
    pages : String,
    lang : String,
    size : String,
    extension : String,
    mirrors : String,
    edit : String,
    md5 : String,
}

impl GenResult {

    fn iter_item_to_string(item : &SSelectItem<'_,'_>) -> String {
        item.text().collect::<Vec<_>>().into_iter().collect()
    }

    fn parse_md5(item : &SSelectItem<'_,'_>, md5_select : &str) -> String {
        let md5_selector = scraper::Selector::parse(md5_select).unwrap();
        let md5_iter = item.select(&md5_selector);
        let mut md5 = String::new();
        for i in md5_iter {
            if let Some(s) = i.value().attr("href") {
                let md5_string = s.split("=").collect::<Vec<&str>>();
                md5.push_str(md5_string[1]);
                break
            }
            else {
                md5.push_str("no md5");
            }
        }
        md5
    }

    // pub fn from(vector : Vec<&'a str>) -> GenResult<'a> {
    pub fn from(item : &SSelectItem) -> GenResult {
        let split : String = str::replace(&GenResult::iter_item_to_string(item),"\t","");
        let vector = split
            .split("\n")
            .map(|x| x.trim())
            .collect::<Vec<&str>>();

        let md5_select = format!(r#"a[id="{}"]"#,vector[0]);

        GenResult {
            id : vector[0].to_owned(),
            author : vector[1].to_owned(),
            title : vector[2].to_owned(),
            publisher : vector[3].to_owned(),
            year : vector[4].to_owned(),
            pages : vector[5].to_owned(),
            lang : vector[6].to_owned(),
            size : vector[7].to_owned(),
            extension : vector[8].to_owned(),
            mirrors : vector[9].to_owned(),
            edit : vector[10].to_owned(),
            md5 : GenResult::parse_md5(item,&md5_select),
        }
    }

    pub fn md5(&self) -> &str {
        &self.md5
    }

    pub fn bibtex(&self) -> String {
        format!("http://gen.lib.rus.ec/book/index.php?md5={}&oftorrent=", self.md5)
    }

}

impl Torrent for GenResult {
    fn torrent(&self) -> String {
        format!("http://gen.lib.rus.ec/book/index.php?md5={}&oftorrent=", self.md5)
    }
}

impl <'a>Torrent for &'a GenResult {
    fn torrent(&self) -> String {
        format!("http://gen.lib.rus.ec/book/index.php?md5={}&oftorrent=", self.md5)
    }
}

impl Rowable for GenResult {

    fn header() -> Row {
        row![Fr-> "#", Fy->"Size", Fg->"Ext", Fb->"Title", Fm->"Author",Fm->"Pages" ]
    }

    fn col_values(&self) -> [&String; 5] {
        [&self.size, &self.extension, &self.title, &self.author, &self.pages]
    }

}


impl Printable for GenResult {
    fn print(&self) {

        fn truncate(string : &String, limit : usize) -> String {
            let title_str = &string[..];
            let title_vec = UnicodeSegmentation::graphemes(title_str, true).collect::<Vec<&str>>();
            if title_vec.len() >= limit {
                title_vec[..limit].join("")
            }
            else {
                title_vec.join("")
            }
        }

        let title = truncate(&self.title, 75usize);
        let row = format!("{size} {ext} | {tit} [{aut}] ({pag}){sep}",
        size = self.size,
        ext = self.extension,
        tit = title,
        aut = self.author,
        pag = self.pages,
        sep = "\n----------------------------------------------------",
        );
        println!("{}", row);
    }
}

/// The Tableable trait implemented for Page<GenResult>
/// Returns a Table which can then be printed
impl <T>Tableable for bucaneer::types::Page<T>
where T : Rowable {

    type TRow = T;

    fn tablify(&self) -> Table {

        // This works but it's probably better to use the formatted width
        // fn truncate(string : &str, limit : usize) -> String {
        //     let slice = &string[..];
        //     let _vec = UnicodeSegmentation::graphemes(slice, true).collect::<Vec<&str>>();
        //     if _vec.len() >= limit {
        //         _vec[..limit].join("")
        //     }
        //     else {
        //         _vec.join("")
        //     }
        // }

        // fn truncate_array(meta : &[&String;5]) -> [String;5] {
        //     let mut meta_trunc = [String::from(""),String::from(""),String::from(""),String::from(""),String::from("")];
        //     for (m, mt) in meta.iter().zip(meta_trunc.iter_mut()) {
        //         *mt = truncate(&m[..], 60usize).clone();
        //     }
        //     meta_trunc
        // }

        fn format_into_width(string : &str, width : usize) -> String {
            let word_vec = string.unicode_words().collect::<Vec<&str>>();
            let mut split_vec : Vec<&str> = Vec::new();
            let mut count : usize = 0;
            for word in word_vec {
                let ng_p_word = word.graphemes(true).count();
                if count + ng_p_word <= width {
                    // if the new word adds less than width
                    split_vec.push(word); // add word to final_vec
                    count += ng_p_word; // update counter
                }
                else {
                    // if not, it means the word goes over "width" graphemes
                    split_vec.push("\n"); // add a breakline
                    split_vec.push(word); // add new word
                    count = ng_p_word; // reset counter
                }
            }

            let joined_sentence = split_vec.join(" ");
            joined_sentence
        }

        fn format_into_width_array(meta : &[&String;5], sizes : [usize;5]) -> [String;5] {
                let mut meta_trunc = [String::from(""),String::from(""),String::from(""),String::from(""),String::from("")];
                for ((i,m), mt) in meta.iter().enumerate().zip(meta_trunc.iter_mut()) {
                    *mt = format_into_width(&m[..], sizes[i]).clone();
                }
                meta_trunc
        }

        let mut table = Table::new();
        table.add_row(Self::TRow::header());

        for (mut i, r) in self.into_iter().enumerate() {
            /* size     => meta_trunc[0]
             * ext      => meta_trunc[1]
             * title    => meta_trunc[2]
             * author   => meta_trunc[3]
             * pages    => meta_trunc[4]*/
            let meta_trunc = format_into_width_array(&r.col_values(), [2,10,70,15,8]);
            let mut rowtext =  i.to_string();
            table.add_row(row![Fr-> rowtext, Fy->meta_trunc[0], Fg->meta_trunc[1], Fb->meta_trunc[2], Fm->meta_trunc[3], Fm->meta_trunc[4] ]);
        }
        table
    }
} // end impl Tableable for Page


/// Implementation of Printable for Page<GenResult>
impl Printable for bucaneer::types::Page<GenResult> {
        fn print(&self) {
            let s_row = 2+self.display_size()*(self.number()-1); //
            let top_bound =  s_row + self.display_size();
            let e_row = if top_bound < self.size() {top_bound} else {self.size()};
            let table = self.tablify();
            let mut t = Table::new();
            for row in table.slice(s_row..e_row).row_iter() {
                t.add_row(row.clone());
            }
            t.printstd();
        }
} // end impl Tableable for Page
