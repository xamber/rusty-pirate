extern crate bucaneer;
extern crate reqwest;
extern crate scraper;
extern crate unicode_segmentation;
#[macro_use]
extern crate prettytable;

mod types;
use std::process;
use std::env;
use bucaneer::{RequestResult};

fn main() {

    let args : Vec<String> = env::args().collect();
    // let program = args[0].clone();
    let searh_string = args[1..].join("+");

    let html_request = format!("http://gen.lib.rus.ec/search.php?req={}&lg_topic=libgen&open=0&view=simple&res=25&phrase=0&column=def", searh_string);
    let request = RequestResult::request_errorhandler(&html_request);

    let mut html_string : String = String::new();
    if let RequestResult::Found(s) = request {
        html_string.push_str(&s);
    }

    let mut result_page = match scrap::scrap_results(&html_string) {
        Some(r) => {println!("{:?}", r); r}
        None    => { println!("No results"); process::exit(0)},
    };

    interaction::start_user_input(&mut result_page);

}

mod interaction {
    use bucaneer;
    use bucaneer::{types::Page};
    use bucaneer::command::Action;
    use types::Printable;
    use types::GenResult;
    use std::io::{self, Write};
    use std::process::Output;

    #[derive(PartialEq)]
    enum Status {
        NotDone,
        Done,
        Helping,
        Error(Vec<Output>),
    }

    type G = GenResult;

    fn ps1(page : &Page<G>) {
        print!("Page {}/{}, enter ? for help: ",
               page.number(), page.total_pages());
        io::stdout().flush().unwrap();
    }

    fn frame(page : &Page<G>) {
        print!("{}[2J", 27 as char); // clear screen
        page.print();
        ps1(page);
    }

    pub fn start_user_input(page : &mut Page<G>) {
        frame(page);
        loop {
            let input = bucaneer::parse::read_input_from_cli();
            match loop_function(page, &input) {
                Status::Helping => {},
                Status::Error(e) => println!("{:?}", e),
                Status::NotDone => frame(page),
                Status::Done => break,
            }
        }
    }

    fn loop_function(page : &mut Page<G>, input : &str) -> Status {
        match Action::parse_input(&input) {
            Action::Next => {page.next(); Status::NotDone},
            Action::Prev => {page.prev(); Status::NotDone},
            Action::Info(n) => {Status::NotDone},
            Action::Quit => {Status::Done},
            Action::Help => {Action::print_help();ps1(page); Status::Helping},
            Action::NumberList(nl) => {
                let mut op = true;
                let mut evec = Vec::new();
                for result in bucaneer::command::send_to_torrent(nl, page.nc_iter()) {
                    match result {
                        Ok(out) => match out.status.success() {
                            true => op = op & true,
                            false => {op = op & false; evec.push(out)},
                        }
                        Err(_e) => {op = false},
                    }
                }
                if op == true { Status::NotDone } else { Status::Error(evec) }
            },
            Action::Unknown(_u) => {Action::print_help();ps1(page); Status::Helping},
        }
    }
}

mod scrap {
use bucaneer::{Filter, types::Page};
use types::GenResult;

    pub fn scrap_results(html_string : &str) -> Option<Page<GenResult>> {

        let filter : Filter = Filter::from(html_string, r#"tr[valign="top"]"#);

        let mut result_page : Page<GenResult> = Page::new(10usize);

        for i in filter.select().skip(1) {
            let result = GenResult::from(&i);
            result_page.add_result(result);
        }

        if result_page.is_empty() {
            None
        }
        else {
            Some(result_page)
        }
    }
}
