#[macro_use]
extern crate prettytable;

extern crate reqwest;

extern crate select;

extern crate scraper;

#[macro_use]
extern crate clap;

extern crate itertools;

extern crate bucaneer;

extern crate unicode_segmentation;

mod scrap; // core functionality
mod command; // user interaction
mod mirror; // mirror functionality
mod types; //core types

use clap::{Arg, App, AppSettings, SubCommand};

fn main() {
    let about : String = String::from("Rusty Pirate yarrrrll! ") + vars::PIRATE_SHIP;
    let version = format!("{}.{}",crate_version!(),env!("GIT_COMMIT_HASH_SHORT"));

    let matches = App::new("rusty-pirate")
        .version(&version[..])
        .author("Xamber <xamber@disroot.org>")
        .about(&about[..])
        .subcommand(
            SubCommand::with_name("sail")
            .about("Sails in search of booty!")
            .setting(AppSettings::TrailingVarArg)
            .arg(Arg::with_name("dwldir")
                 .short("w")
                 .long("dwldir")
                 .takes_value(true)
                 .help("Optional download dir for this session")
                 )
            .arg(Arg::with_name("query")
                 .takes_value(true)
                 .required(true)
                 .multiple(true)
                 .help("Search query text"))
            )
        .subcommand(
            SubCommand::with_name("refresh")
            .about("Refreshens our mirrum barrels, yo-ho!")
            .arg(Arg::with_name("proxylist")
                 .help("Updates mirror list"))
            )
        .get_matches();

    match matches.subcommand() {
        ("sail",    Some(sub_c)) => scrap::sail_subcommand(sub_c),
        ("refresh", Some(sub_c)) => scrap::refresh_subcommand(sub_c),
        ("",        None       ) => println!("No subcommand was used"),
        _           => println!("Some other subcommand was used"),
    }
}

mod vars {
    pub static PIRATE_SHIP : &str = "
              4
             /|   4
      r-_*  /_|  /|
      l###\\___|___|_/*-
   ~~~     ~ o o~o~ ~   ~~    ~~        
 ~~   ~~~   ~~~  ~ ~~  ~  ~~            
";
}

