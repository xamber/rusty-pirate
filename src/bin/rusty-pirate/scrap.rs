use std::fs;
use std::process;
use std::path;
use scraper::{Html, Selector};
use clap;

use mirror;
use command;
use types::{TitleMagnet};
use bucaneer;
use bucaneer::types::Page;

fn parse_html(s_html: &str) -> Option<Page<TitleMagnet>> {

    // parases string into 
    let filter = bucaneer::Filter::from(&s_html,r#"div[class="detName"]"#); // selects result rows

    let mut results : Vec<TitleMagnet> = Vec::new();

    for item in filter.select() {
        let mut torrent = TitleMagnet::from(&item);
        results.push(torrent);
    }

    let results : Page<TitleMagnet> = Page::from(10usize, results);

    if results.size() > 0 {
        Some(results)
    }
    else {
        None
    }
}

pub fn refresh_subcommand(super_match: &clap::ArgMatches) {

    let s_url = match super_match.value_of("proxylist")  {
        Some(url) => url,
        None => "https://piratebay-proxylist.com/",
        // "https://proxybay.bz/"
    };

    let s_html = mirror::make_request(s_url).unwrap().text().unwrap();
    let fragment = Html::parse_fragment(&s_html);

    // let s_html = bucaneer::RequestResult::request_errorhandler(s_url);

    // // parases string into 
    // let fragment = match s_html {
    //     bucaneer::RequestResult::Found(s) => Html::parse_fragment(s),
    //     _ => ""
// }

    // parses based on a CSS selector
    let mirror_selector = Selector::parse(r#"a[rel="nofollow"]"#).unwrap();
    let mirror_iter = fragment.select(&mirror_selector); //.collect::<Vec<_>>();

    let mut w_string = String::new();
    for mirror in mirror_iter {
        let mirror_link = mirror.value().attr("href").unwrap().to_string();
        w_string.push_str(&mirror_link);
        w_string.push_str("\n");
    }

    println!("Fresh mirrors Sir!");

    let config_path = path::Path::new(env!("CONFIG_DIR"));
    fs::write(config_path.join("mirror.list"),w_string).expect("Unable to write file")

}

pub fn sail_subcommand(super_match : &clap::ArgMatches) {

    let query :  Vec<&str> = match super_match.values_of("query") {
        Some(query) => {println!("Aye aye, Captn'!"); query.collect()},
        None    => {println!("Ma' Captn', we need a search string to start with, arrr!");process::exit(1)},
    };

    let query_string : String = query.join(" ");
    let s_html = mirror::get_html_from_mirrorlist(&query_string);
    // let s_html = fs::read_to_string("test.html").expect("Hay quilombo para leer el test.html");

    let mut result_page : Page<TitleMagnet> = match parse_html(&s_html) {
        Some(v) => v,
        None    => {println!("No results"); process::exit(1)},
    };

    command::start_user_input(&mut result_page);
}
