use std::fs;
use reqwest;
use std::path;

#[derive(PartialEq)]
enum MirrorResult {
    Found(String),
    NotFound,
}

impl MirrorResult {
    fn r_handler(response : &mut reqwest::Response ) -> MirrorResult {
        match response.status() {
            reqwest::StatusCode::NotFound => {
                println!("Arriving at 404 sir, yer not lost arn't ye?");
                MirrorResult::NotFound
            },
            reqwest::StatusCode::Unregistered(u) => {
                println!("Poseidon's beard! wut is {:?}?' ",u);
                println!("Possible server error");
                MirrorResult::NotFound
            }
            _ => {
                let response_text  = response.text().unwrap();
                MirrorResult::Found(response_text)
            }
        }
    }

    fn e_handler(error: reqwest::Error) -> MirrorResult {
        println!("Wi'v lost'em in the storm"); 
        println!("error := {:?}", error.get_ref());
        if error.is_http() {
            match error.url() {
                None => println!("No Url given"),
                Some(url) => println!("Problem making request to: {}", url),
            }
        }
        // Inspect the internal error and output it
        if error.is_serialization() {
            if let Some(err) = error.get_ref() {
                println!("problem parsing information {}", err)
            }
        }
        if error.is_redirect() {
            println!("server redirecting too many times or making loop");
        }
        MirrorResult::NotFound
    }

    pub fn request_errorhandler(query_url: &str) -> MirrorResult {
        // To read locally 
        // use mirror;
        // use std::fs;
        // let r_html_file = fs::read_to_string(url);
        // let html : String = mirror::read_mirror(&r_html_file);
        // let s_html : &str = &html;
        //let  mirror_try : MirrorResult = 

        match make_request(&query_url) {
            Ok(mut response) => MirrorResult::r_handler(&mut response),
            Err(e) => MirrorResult::e_handler(e),
        }
    }
}


struct MirrorList(Vec<String>);

impl MirrorList {
    pub fn new(file : &str) -> MirrorList {
        let mut r_mirror = fs::read_to_string(file).unwrap();
        let lines = r_mirror.as_mut_str().lines().map(|x| x.to_owned()).collect();
        MirrorList(lines)
    }
}

pub fn make_request(url : &str) -> Result<reqwest::Response, reqwest::Error> {

    let rpolicy = reqwest::RedirectPolicy::limited(1usize);

    let client = reqwest::Client::builder()
        .redirect(rpolicy)
        .build()?;

    let client_get = client.get(url).send();

    client_get
}

fn turn_string_to_query_url(query : &str, mirror : &str) -> String {
    let query_url : String = String::new();
    query_url + &mirror + "/search/" + &query + "/0/99/0"   // By relevance
    // query_url + &mirror + "/search/" + &query + "/1/3/0" // By date
    // query_url + "https://" + &mirror + "/search/" + &query + "/0/99/0"
}


pub fn get_html_from_mirrorlist( query_string : &str  ) -> String {

    let config_path = path::Path::new(env!("CONFIG_DIR"));
    let mirrorlist : MirrorList = MirrorList::new(
        config_path.join("mirror.list")
        .to_str()
        .expect("Can't find mirror.list")
    );
    let mut s_html = String::new();

    for m in mirrorlist.0.iter() {

        let mirror = m;

        let query_url = turn_string_to_query_url(&query_string, &mirror);
        println!("Wi'v set course to:\n \"{}\"", query_url);

        let searching = MirrorResult::request_errorhandler(&query_url);

        if let MirrorResult::Found(s) = searching {
            s_html.push_str(&s);
            break
        }
    }

    s_html
}
