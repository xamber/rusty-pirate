use scraper;
use scraper::element_ref::ElementRef;
use std::str;
use prettytable::{Table, Slice, row::Row};
use unicode_segmentation::UnicodeSegmentation;
use bucaneer;
use bucaneer::RequestResult;
use bucaneer::Torrent;

type SSelect<'a,'b> = scraper::html::Select<'a,'b>;
type SSelectItem<'a,'b> = <SSelect<'a,'b> as Iterator>::Item;

pub trait Rowable {
    fn header() -> Row;
    fn col_values(&self) -> [&String; 5];
}

pub trait Tableable {
    type TRow;
    fn tablify (&self) -> Table;
}

pub trait Printable {
    fn print(&self);
}

pub trait PrintTorrentInfo {
    fn print_info(&self, &[usize]);
}

#[derive(Debug)]
pub struct TitleMagnet {
    title: String,
    magnet: String,
    url: String,
    date: String,
    size: String,
    user: String,
    seeds: String,
    leechs: String
}

impl TitleMagnet {

    pub fn new() -> TitleMagnet {
        TitleMagnet{
            title: String::new(),
            magnet: String::new(),
            url: String::new(),
            date: String::new(),
            size: String::new(),
            user: String::new(),
            seeds: String::new(),
            leechs: String::new()
        }
    }

    fn iter_item_to_string(item : &SSelectItem<'_,'_>) -> String {
        item.text().collect::<Vec<_>>().into_iter().collect()
    }

    fn split_date_size_auth(meta : &str) -> [String;3] {

        fn extract_value(lit : &str, i : usize) -> &str {
            let end = lit
                .trim()
                .split(" ")
                .filter(|x| ! x.is_empty())
                .collect::<Vec<&str>>();
            end[i]
        }

        let date_n_size_auth : Vec<&str> = meta
            .split(",")
            .map(|x| x.trim())
            .collect::<Vec<&str>>();

        let date : &str = extract_value(date_n_size_auth[0], 1);
        let size : &str = extract_value(date_n_size_auth[1], 1);
        let user : &str = extract_value(date_n_size_auth[2], 2);

        [date.replace("\u{a0}"," "), size.replace("\u{a0}"," "),user.to_string()]
    }

    // fn parse_magnet(item : &SSelectItem<'_,'_>) -> String {
    fn parse_subnode_attr<'a>(item : &'a SSelectItem<'_,'_>, filter : &str, attr : &str ) -> Option<String> {
        let trow_html = item.html();
        let magnet_filter = bucaneer::Filter::from(&trow_html,filter);
        // let magnet_filter = bucaneer::Filter::from(&trow_html,r#"a[title="Download this torrent using magnet"]"#);
        let magnet_iter = magnet_filter.select();
        let mut magnet_pruned : Vec<&str> = magnet_iter.filter_map( |x| x.value().attr(attr)).collect();
        magnet_pruned.pop().map( |x| x.to_string() )
    }

    pub fn from(item : &SSelectItem) -> TitleMagnet {
        // We find the parent node based on an element we know the result has.
        // We use the parent to scrap the other values.
        let tr = ElementRef::wrap(item.parent().unwrap().parent().unwrap()).unwrap();

        let magent = TitleMagnet::parse_subnode_attr(&tr, r#"a[title="Download this torrent using magnet"]"#, "href").unwrap();
        let url = TitleMagnet::parse_subnode_attr(&tr, r#"a[class="detLink"]"#, "href").unwrap();

        let split : String = str::replace(&TitleMagnet::iter_item_to_string(&tr),"\t","");
        let vector = split
            .split("\n")
            .map(|x| x.trim())
            .filter(|x| ! x.is_empty())
            .collect::<Vec<&str>>();

        let d_s_a = TitleMagnet::split_date_size_auth(vector[3]);

        TitleMagnet {
            title : vector[2].to_string(),
            magnet : magent,
            url : url,
            date : d_s_a[0].to_string(),
            size : d_s_a[1].to_string(),
            user : d_s_a[2].to_string(),
            seeds : vector[4].to_string(),
            leechs : vector[5].to_string(),
        }
    }

    pub fn magnet(&self) -> &String {
        &self.magnet
    }

    pub fn title(&self) -> &String {
        &self.title
    }

    pub fn get_info(&self) -> Option<String> {
        let s_html = match RequestResult::request_errorhandler(&self.url)  {
            RequestResult::Found(s) => s,
            _                       => "".to_owned(),};
        let filter = bucaneer::Filter::from(&s_html,r#"pre"#); // selects result rows
        let results : Option<String> = filter.select().map(|x| x.text().collect()).collect::<Vec<String>>().pop();
        // println!("{:?}", results);
        results
    }

}

impl Torrent for TitleMagnet {
    fn torrent(&self) -> String {
        format!["{}",&self.magnet]
    }
}

impl <'a>Torrent for &'a TitleMagnet {
    fn torrent(&self) -> String {
        format!["{}",&self.magnet]
    }
}

impl Rowable for TitleMagnet {

    fn header() -> Row {
    row![Fr-> "#", Fy->"Size", Fg->"Date", Fb->"Name", Fm->"S",Fm->"L" ]
    }

    fn col_values(&self) -> [&String; 5] {
        [&self.size, &self.date, &self.title, &self.seeds, &self.leechs]
    }

}

/// Implementation of Printable for TitleMagnet
impl Printable for TitleMagnet {
    fn print(&self) {
        
    }
}

/// The Tableable trait implemented for Page<T>
/// Returns a Table which can then be printed
impl <T>Tableable for bucaneer::types::Page<T>
where T : Rowable {

    type TRow = T;

    fn tablify(&self) -> Table {

        fn format_into_width(string : &str, width : usize) -> String {
            let word_vec = string.unicode_words().collect::<Vec<&str>>();
            let mut split_vec : Vec<&str> = Vec::new();
            let mut count : usize = 0;
            for word in word_vec {
                let ng_p_word = word.graphemes(true).count();
                if count + ng_p_word <= width {
                    // if the new word adds less than width
                    split_vec.push(word); // add word to final_vec
                    count += ng_p_word; // update counter
                }
                else {
                    // if not, it means the word goes over "width" graphemes
                    split_vec.push("\n"); // add a breakline
                    split_vec.push(word); // add new word
                    count = ng_p_word; // reset counter
                }
            }

            let joined_sentence = split_vec.join(" ");
            joined_sentence
        }

        fn format_into_width_array(meta : &[&String;5], sizes : [usize;5]) -> [String;5] {
                let mut meta_trunc = [String::from(""),String::from(""),String::from(""),String::from(""),String::from("")];
                for ((i,m), mt) in meta.iter().enumerate().zip(meta_trunc.iter_mut()) {
                    *mt = format_into_width(&m[..], sizes[i]).clone();
                }
                meta_trunc
        }

        let mut table = Table::new();
        table.add_row(Self::TRow::header());

        for (mut i, r) in self.into_iter().enumerate() {
            /* size     => meta_trunc[0]
             * ext      => meta_trunc[1]
             * title    => meta_trunc[2]
             * author   => meta_trunc[3]
             * pages    => meta_trunc[4]*/
            let meta_trunc = format_into_width_array(&r.col_values(), [20,20,70,15,8]);
            let mut rowtext =  i.to_string();
            table.add_row(row![Fr-> rowtext, Fy->meta_trunc[0], Fg->meta_trunc[1], Fb->meta_trunc[2], Fm->meta_trunc[3], Fm->meta_trunc[4] ]);
        }
        table
    }
} // end impl Tableable for Page


/// Implementation of Printable for Page<GenResult>
impl Printable for bucaneer::types::Page<TitleMagnet> {
        fn print(&self) {
            let s_row = 1+self.display_size()*(self.number()-1); //
            let top_bound =  s_row + self.display_size();
            let e_row = if top_bound < self.size() {top_bound} else {self.size()};
            let table = self.tablify();
            let mut t = Table::new();
            for row in table.slice(s_row..e_row).row_iter() {
                t.add_row(row.clone());
            }
            t.printstd();
        }
} // end impl Tableable for Page

/// Implementation of PrintTorrentInfo for Page<GenResult>
impl PrintTorrentInfo for bucaneer::types::Page<TitleMagnet> {
    fn print_info (&self, numlist : &[usize]) {

        fn give_format(s : &str) -> String {
            let mut lines = s.split('\n').peekable();
            let mut formatted_string = String::new();
            while let Some(l) = lines.next() {
                formatted_string.push_str("    |");
                formatted_string.push_str(l);
                if None != lines.peek() {
                    formatted_string.push('\n');
                }
            }
            formatted_string
        }

        let separator_top   = "____________________________________";
        let separator_title = "----#-------------------------------";
        let separator_bot   = "    #===============================";

        let list = self.torrent_list();
        for i in numlist {
            let info : Option<String> = list[*i].get_info();
            match info {
                Some(s) => println!("{}\n{}\n{}\n{}\n{}",
                                    separator_top,
                                    list[*i].title(),
                                    separator_title,
                                    give_format(&s),
                                    separator_bot),
                None    => println!("No info, sorry"),
            }
        }
    }
} // end impl TorrentInfo for Page
