use bucaneer;
use bucaneer::{types::Page};
use bucaneer::command::Action;
use types::Printable;
use types::TitleMagnet;
use std::io::{self, Write};
use std::process::Output;
use types::PrintTorrentInfo;

#[derive(PartialEq)]
enum Status {
    NotDone,
    Done,
    Helping,
    Error(Vec<Output>),
}

type G = TitleMagnet;

fn ps1(page : &Page<G>) {
    print!("Page {}/{}, enter ? for help: ",
           page.number(), page.total_pages());
    io::stdout().flush().unwrap();
}

fn frame(page : &Page<G>) {
    print!("{}[2J", 27 as char); // clear screen
    page.print();
    ps1(page);
}

pub fn start_user_input(page : &mut Page<G>) {
    frame(page);
    loop {
        let input = bucaneer::parse::read_input_from_cli();
        match loop_function(page, &input) {
            Status::Helping => {},
            Status::Error(e) => println!("{:?}", e),
            Status::NotDone => frame(page),
            Status::Done => break,
        }
    }
}

fn loop_function(page : &mut Page<G>, input : &str) -> Status {
    match Action::parse_input(&input) {
        Action::Next => {page.next(); Status::NotDone},
        Action::Prev => {page.prev(); Status::NotDone},
        Action::Quit => {Status::Done},
        Action::Help => {Action::print_help();ps1(page); Status::Helping},
        Action::Info(n) => {
            page.print_info(&n);
            // println!("{:?}",n);
            Status::Helping
        },
        Action::NumberList(nl) => {
            let mut op = true;
            let mut evec = Vec::new();
            for result in bucaneer::command::send_to_torrent(nl, page.nc_iter()) {
                match result {
                    Ok(out) => match out.status.success() {
                        true => op = op & true,
                        false => {op = op & false; evec.push(out)},
                    }
                    Err(_e) => {op = false},
                }
            }
            if op == true { Status::NotDone } else { Status::Error(evec) }
        },
        Action::Unknown(_u) => {Action::print_help();ps1(page); Status::Helping},
    }
}

